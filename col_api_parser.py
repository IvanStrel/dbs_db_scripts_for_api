# coding: utf-8
import json
import requests


def col_query(genera, species, infra_name, infra_marker, author):
    """
    Function to make a query to COL API and to parse results
    :param genera: genera scientific name
    :param species: species scientific name
    :param infra_name: infraspecies scientific name
    :param infra_marker: infraspecies marker
    :param author: string with taxon author abbreviation
    :return: dictionary with results. Contains elements 'parser status' and 'parser_error'
        and, possibly, resulting elemets from COL response
    """
    # Convert all params to lowercase and remove whitespaces
    genera = genera.lower().replace(" ", "")
    species = species.lower().replace(" ", "")
    infra_name = infra_name.lower().replace(" ", "")
    infra_marker = infra_marker.lower().replace(" ", "")

    # Capitalize genera
    genera = genera.capitalize()

    # Add + sign before infra name
    infra_name_p = ""
    if len(infra_name) > 0:
        infra_name_p = "+" + infra_name

    # Prepare url for query
    url_query = "http://www.catalogueoflife.org/col/webservice?name={}+{}{}&format=json&response=full"
    url_query = url_query.format(genera, species, infra_name_p)

    # Make API query
    try:
        resp = requests.get(url_query, timeout=15)
        resp_j = json.loads(resp.text)
    except requests.ConnectionError:
        result = {"parser_status": "Error", "parser_error": "ConnectionError"}
        return result
    except requests.HTTPError:
        result = {"parser_status": "Error", "parser_error": "HTTPError"}
        return result
    except requests.Timeout:
        result = {"parser_status": "Error", "parser_error": "Timeout"}
        return result
    except requests.TooManyRedirects:
        result = {"parser_status": "Error", "parser_error": "TooManyRedirects"}
        return result
    except:
        result = {"parser_status": "Error", "parser_error": "UnexpectedError"}
        return result

    '''
    Response parsing procedure:
        1. Check for "error_message" in response (j["error_message"])
            if true: return error_message
        2. Check for full match
            if true: return result
        3. Check for match without Author
            if none:
                return Error: unmatched infra_name
            if one match:
                return matched result with Warning: unresolved author
            if many:
                return Error: Homonym, unresolved author
    '''

    # Check for error_message in response
    if len(resp_j["error_message"]) > 0:
        result = {"parser_status": "Error", "parser_error": resp_j["error_message"]}
        # print("Error in results: " + result["parser_error"])
        return result

    # ~~~~~~~~~~~~~~~~~~~~
    #   Main parsing
    # ~~~~~~~~~~~~~~~~~~~~

    # Prepare list of names (input and COL answers) for comparison
    target_str = [genera, species, infra_name, infra_marker, author]
    res_str = [[x["genus"],
                x["species"],
                x["infraspecies"],
                x["infraspecies_marker"],
                x["author"]] for
               x in resp_j['results']]

    # Look for full match
    if target_str in res_str:
        idx = res_str.index(target_str)
        result = resp_j['results'][idx]

        # Check if name is Accepted or Synonym
        if result["name_status"] in ["accepted name", "provisionally accepted name"]:
            result.update({"parser_status": "Accepted", "parser_error": ""})
            # print(result["name_html"] + " parser status: " + str(result["parser_status"]))
            return result
        else:
            result = result["accepted_name"]
            result.update({"parser_status": "Synonym", "parser_error": ""})
            # print(result["name_html"] + " parser status: " + str(result["parser_status"]))
            return result

    # Here full match not found. Hence, look for partial match without author
    target_str = [genera, species, infra_name, infra_marker]
    res_str = [[x["genus"],
                x["species"],
                x["infraspecies"],
                x["infraspecies_marker"]] for
               x in resp_j['results']]

    # Get indices of elements in res_str matching target_str
    occurrences = [i for i, x in enumerate(res_str) if x == target_str]

    # If there is no match elements, than some problem with infraspecies_marker
    if len(occurrences) == 0:
        infr = [obj["infraspecies_marker"] for
                obj in
                resp_j['results'] if
                obj["genus"] == genera and
                obj["species"] == species and
                obj["infraspecies"] == infra_name]
        err = "Unresolved infraspecies marker. Variants: " + str(infr)
        result = {"parser_status": "Error", "parser_error": err}
        return result

    # If there is only one match, we can assume misspelling in author name
    if len(occurrences) == 1:
        result = resp_j['results'][occurrences[0]]
        if result["name_status"] in ["accepted name", "provisionally accepted name"]:
            result.update({"parser_status": "Accepted", "parser_error": "!Unresolved author"})
            return result
        else:
            try:
                result = result["accepted_name"]
                result.update({"parser_status": "Synonym", "parser_error": "!Unresolved author"})
                return result
            except:
                result = {"parser_status": "Error", "parser_error": "Unexpected error"}
                return result

    # If there are more then one match we can assume Homonim
    if len(occurrences) > 1:
        auth_sub = [obj["author"] for obj in [resp_j['results'][i] for i in occurrences]]
        result = {"parser_status": "Error",
                  "parser_error": "Possible Homonym, unresolved author. Variants: " + str(auth_sub)}
        return result
