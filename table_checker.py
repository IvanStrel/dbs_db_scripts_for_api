# coding: utf-8
from col_api_parser import col_query
from red_list_api_parser import redlist_query
from tnrs_api_parser import trns_query
from sys import argv, stdout
import codecs

import csv
import os

def read_check_write(file_in, file_out = ""):
    """
    Function which will read csv,
    check species names against COL, RedList and TRNS,
    and will save result to new file.
    :param file_in: csv table with species names (cols: genera, species, author, infraspecies, infra_mark)
    :param file_out: name of output csv file with results of names checking
    :return: None.
    """

    # Check if output file name is provided or construct it basing on input name + _out:
    in_name, in_extension = os.path.splitext(file_in)
    file_out = file_out if len(file_out) > 0 else in_name + "_out" + in_extension

    # Read csv file
    in_tab = []
    with open(file_in, "r", encoding='utf-8', newline='') as csvfile:
        csv_reader = csv.reader(csvfile)
        for row in csv_reader:
           in_tab.append(row)


    out_tab = []
    # Prepare headers
    out_header = ["col_статус", "доп_информация", "порядок", "семейство","род", "вид", "внутривидовой эпитет",
                  "внутривидовой таксон", "автор", "культивар",
                  "ответ МСОП", "категория МСОП", "критерий МСОП",
                  "Ответ TRNS", "TRNS варианты замены"]
    out_tab.append(out_header)

    # Main loop
    i = 0
    max_i = len(in_tab)
    for row in in_tab[1:]:
        # Check for clean species names
        if len(row[2]) == 0:
            row[2] = "sp."

        row_out = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]
        col_res = col_query(row[1], row[2], row[3], row[4], row[5])
        if col_res["parser_status"] == "Error":
            row_out[0] = col_res["parser_status"]
            row_out[1] = col_res["parser_error"]

            # If no names fond, launch TRNS service
            if col_res["parser_error"] == "No names found":
                trns_res = trns_query(row[1], row[2], row[3])
                row_out[-2] = trns_res["parser_status"] + trns_res["parser_error"]
                row_out[-1] = str(trns_res["items"]) if trns_res["parser_status"]is not "Error" else ""
        else:
            row_out[0] = col_res["parser_status"]
            row_out[1] = col_res["parser_error"]
            row_out[2] = col_res["classification"][3]["name"]
            row_out[3] = col_res["classification"][4]["name"]
            row_out[4] = col_res["genus"]
            row_out[5] = col_res["species"]
            row_out[6] = col_res["infraspecies"]
            row_out[7] = col_res["infraspecies_marker"]
            row_out[8] = col_res["author"]
            row_out[9] = row[6]

            # Get information from Red List
            rl_res = redlist_query(col_res["genus"], col_res["species"], col_res["infraspecies"])
            if rl_res["parser_status"] != "Error":
                row_out[10] = rl_res["parser_status"] + rl_res["parser_error"]
                row_out[11] = rl_res["category"]
                row_out[12] = rl_res["criteria"]
            else:
                row_out[10] = rl_res["parser_status"] + rl_res["parser_error"]

        # print progress
        i += 1
        stdout.write(str(round(i * 100 / max_i, 2)) + "%" + "      " + "\r")
        stdout.flush()
        out_tab.append(row_out)

    # Combine old and new tables:
    out_tab = [row_1 + row_2 for (row_1, row_2) in zip(in_tab, out_tab)]

    # Write table to output
    with open(file_out, 'w+', encoding='utf-8') as csvfile:
        csv_writer = csv.writer(csvfile, lineterminator='\n')
        for row in out_tab:
            csv_writer.writerow(row)

if __name__ == "__main__":
    out_f = argv[2] if len(argv) > 2 else ""
    read_check_write(argv[1], out_f)
