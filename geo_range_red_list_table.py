# coding: utf-8

from red_list_geo_range_api import redlist_geo_range
from sys import argv, stdout
import codecs

import csv
import os

def geo_range_table(file_in, file_out = ""):
    """
    Function which will read csv,
    check species names against COL, RedList and TRNS,
    and will save result to new file.
    :param file_in: csv table with species names (cols: genera, species, author, infraspecies, infra_mark)
    :param file_out: name of output csv file with results of names checking
    :return: None.
    """
    # Check if output file name is provided or construct it basing on input name + _out:
    in_name, in_extension = os.path.splitext(file_in)
    file_out_wide = file_out if len(file_out) > 0 else in_name + "_out_wide" + in_extension
    file_out_long = file_out if len(file_out) > 0 else in_name + "_out_long" + in_extension

    # Read csv file
    in_tab = []
    with open(file_in, "r", encoding='utf-8', newline='') as csvfile:
        csv_reader = csv.reader(csvfile)
        for row in csv_reader:
            in_tab.append(row)

    # Prepare headers
    out_wide_header = ["статус", "распространение"]

    out_long_header = ["No", "род", "вид", "внутривидовой эпитет",
                       "внутривидовой таксон", "автор", "культивар",
                       "код страны", "страна", "код происхождения"]

    # Add headers to output  tables
    out_tab_wide = [in_tab[0] + (out_wide_header)]
    out_tab_long = []
    out_tab_long.append(out_long_header)

    # Main loop
    i = 0  # iterator for progress calculation
    max_i = len(in_tab)  # max iterator for progress calculation
    for row in in_tab[1:]:
        res1, res2 = redlist_geo_range(row[1], row[2], row[3])
        row_wide_out = ["", ""]

        if res1["parser_status"] == "Error":
            # Compose error row for wide table
            row_wide_out = ["Error", ""]
            row_wide_out = row + row_wide_out
            # Compose error row for long table
            row_long_out = row.append("Error")
        else:
            # Compose normal row for wide table
            row_wide_out[1] = res1["distribution"]
            row_wide_out = row + row_wide_out

            # Compose normal row for long table
            row_long_out = [row[0:7] + x for x in res2]

        # Append rows to output tables
        out_tab_wide.append(row_wide_out)
        out_tab_long.extend(row_long_out)

        # print progress status
        i += 1
        stdout.write(str(round(i * 100 / max_i, 2)) + "%" + "      " + "\r")
        stdout.flush()

    # Write output table
    with open(file_out_wide, 'w+', encoding='utf-8') as csvfile:
        csv_writer = csv.writer(csvfile, lineterminator='\n')
        for row in out_tab_wide:
            csv_writer.writerow(row)

    with open(file_out_long, 'w+', encoding='utf-8') as csvfile:
        csv_writer = csv.writer(csvfile, lineterminator='\n')
        for row in out_tab_long:
            csv_writer.writerow(row)

if __name__ == "__main__":
    out_f = argv[2] if len(argv) > 2 else ""
    geo_range_table(argv[1], out_f)
