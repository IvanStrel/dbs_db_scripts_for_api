# coding: utf-8
from col_api_parser import col_query
from red_list_api_parser import redlist_query
from tnrs_api_parser import trns_query

genera = "Astragalus"
species = "rhizanthus"
infra_name = "candolleanus"
infra_marker = "subsp."
author = "Boiss."

res_col = col_query(genera, species, infra_name, infra_marker, author)
print(res_col["parser_status"])
print(res_col["parser_error"])
print(res_col["name_html"])

#res_rl = redlist_query(genera, species, infra_name)
#print(res_rl)

#res_trns = trns_query(genera, species, infra_name)
#print(res_trns)
