# Parsers for botanical information API services
@ author: Ivan I. Strl'nikov, junior researcher in Donetsk botanical garden
## current services are:
 - Catalogue of life
 - IUCN Red List
 - TNRS iplantcolaborative 

## files in a directory:
**col_api_parser.py** -- function for Catalogue of Life API response parsing.  
Arguments:  

* genera `str`      = genus name
* species `str`     = species name
* infra_name `str`  = infraspecies name
* infra_mark `str`  = infraspecies taxon level abreviation.

Return:

- obj `dict` = contains keys:
    - `["parser_status"]` = Error or of type "Synonym" or "Accepted". Possible Errors = error in REST request, or No_names_found. 
    - `["parser_error"]` = contains error message if _parser_status_ == Error else ""
    - additional keys = if _parser_status_ != Error, contains results form CoL API call. For example `["name"]`, `["genus"]` etc.
    
**red_list_api_parser.py** -- function for IUCN RedList API response parsing.
Should be called only for accepted names.  
Arguments:

* genera `str`      = genus name
* species `str`     = species name
* infra_name `str`  = infraspecies name
   
Return:

- obj `dict` = contains keys:
     - `["parser_status"]` = Error or of type "Synonym" or "Accepted". Possible Errors = error in REST request, or Species not in Red List. 
     - `["parser_error"]` = contains error message if _parser_status_ == Error else ""
     - additional keys = if _parser_status_ != Error, contains results form CoL API call. For example `["category"]`, `["criteria"]` etc.

**tnrs_api_parser.py** -- function for TRNS API response parsing.
Aimed to be used on names which were not found in COL, i.e. with possible misspellings.
Arguments:

- genera `str`      = genus name
- species `str`     = species name
- infra_name `str`  = infraspecies name
   
Return:

- obj `dict` = contains keys:
    - `["parser_status"]` = Error or of type "Synonym" or "Accepted". Possible Errors = error in REST request, or Species not in Red List. 
    - `["parser_error"]` = contains error message if _parser_status_ == Error else ""
    - additional keys = if _parser_status_ != Error, contains results form CoL API call. For example `["accepted_name"]`, `["author"]` etc.

**parsers_work.py** -- temporal script, used for test and demo.

**table_checker.py** -- function for command line use. Utilize functionality of COL, RedList and TNRS parsers.

Usage:

      table_checker.py [arg1] [arg2]

Where:

   - arg1 = path to input CSV file
   - arg2 = path to output CSV file (if not exist, will be created)

Example call:

      $ python table_checker.py "./work/test.csv" "./work/test_out.csv"


Input file must be CSV (separators = ',') with next structure:

No | Род  | Вид          | Внутривидовой эпитет | Внутривидовой таксон | Автор          | Культивар
:--|:-----|:-------------|:---------------------|:---------------------|:---------------|:---------
1  |Ficus | benghalensis | krishnae             | var.                 |(C. DC.) Corner | --       