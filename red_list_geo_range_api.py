# coding: utf-8
import json
import requests


def redlist_geo_range(genera, species, infra_name):
    """
    Function to make a query to Red List API for list of occurrences countries

    :param genera: genera scientific name
    :param species: species scientific name
    :param infra_name: infraspecies scientific name
    :param infra_marker: infraspecies marker
    :return: list of dictionary and list. First dict contains one entree for species and list of countries.
        Second list is a long representation of first list, i.e. one row = one species, one country. I.e. each species
        could be listed meany times (as many as countries).
    """
    # Convert all params to lowercase and remove whitespaces
    genera = genera.lower().replace(" ", "")
    species = species.lower().replace(" ", "")
    infra_name = infra_name.lower().replace(" ", "")

    # Add + sign before infra name
    if len(infra_name) > 0:
        infra_name = " " + infra_name

    # Red_list access token
    token = "d9e7acaad53f9b4d2760858e2091a5a1a3b46a0e52b2b3cf6d6de2274439468f"

    # Prepare url for species query
    url_spec_query = "http://apiv3.iucnredlist.org/api/v3/species/countries/name/{} {}{}?token={}"
    url_spec_query = url_spec_query.format(genera, species, infra_name, token)

    '''
    Get geographical range of the species
    '''
    try:
        resp = requests.get(url_spec_query, timeout=25)
        resp_j = json.loads(resp.text)
    except requests.ConnectionError:
        result = {"parser_status": "Error", "parser_error": "ConnectionError"}
        return result
    except requests.HTTPError:
        result = {"parser_status": "Error", "parser_error": "HTTPError"}
        return result
    except requests.Timeout:
        result = {"parser_status": "Error", "parser_error": "Timeout"}
        return result
    except requests.TooManyRedirects:
        result = {"parser_status": "Error", "parser_error": "TooManyRedirects"}
        return result
    except:
        result = {"parser_status": "Error", "parser_error": "UnexpectedError"}
        return result

    '''
    Parse results
    '''
    country_res = resp_j["result"]
    country = []
    dist_code = []
    code = []
    for i in range(len(country_res)):
        country.append(country_res[i]["country"])
        dist_code.append(country_res[i]["distribution_code"])
        code.append(country_res[i]["code"])

    '''
    Compose first dict (one species, many countries)
    '''

    # Compose first dict
    distr = [x + '(' + y + ')' for x, y in zip(country, dist_code)]
    distr_str = ''
    for unit in distr:
        if len(distr_str) > 0:
            sep = ', '
        else:
            sep = ""
        distr_str += sep + unit

    dict1 = {"parser_status": "", "parser_error": "",
             "genera": genera, "species": species, "infra_name": infra_name,
             "distribution": distr_str}

    # Compose second dict (one species, one country)
    distr = [[x, y ,z] for x, y, z in zip(code, country, dist_code)]

    # Return results
    return dict1, distr