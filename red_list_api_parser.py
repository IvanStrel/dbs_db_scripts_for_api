# coding: utf-8
import json
import requests

def redlist_query(genera, species, infra_name):
    """
    Function to make a query to Red List API and to parse results
    :param genera: genera scientific name
    :param species: species scientific name
    :param infra_name: infraspecies scientific name
    :param infra_marker: infraspecies marker
    :return: dictionary with results. Contains elements 'parser status' and 'parser_error'
        and, possibly, resulting elemets from RedList API response
    """
    # Convert all params to lowercase and remove whitespaces
    genera = genera.lower().replace(" ", "")
    species = species.lower().replace(" ", "")
    infra_name = infra_name.lower().replace(" ", "")

    # Add + sign before infra name
    if len(infra_name) > 0:
        infra_name = " " + infra_name

    # Red_list access token
    token = "d9e7acaad53f9b4d2760858e2091a5a1a3b46a0e52b2b3cf6d6de2274439468f"

    # Prepare url for species query
    url_spec_query = "http://apiv3.iucnredlist.org/api/v3/species/{} {}{}?token={}"
    url_spec_query = url_spec_query.format(genera, species, infra_name, token)

    # Prepare url for citation query
    url_cit_query = "http://apiv3.iucnredlist.org/api/v3/species/citation/{} {}{}?token={}"
    url_cit_query = url_cit_query.format(genera, species, infra_name, token)

    '''
    Get information about species
    '''
    try:
        resp = requests.get(url_spec_query, timeout=15)
        resp_j = json.loads(resp.text)
    except requests.ConnectionError:
        result = {"parser_status": "Error", "parser_error": "ConnectionError"}
        return result
    except requests.HTTPError:
        result = {"parser_status": "Error", "parser_error": "HTTPError"}
        return result
    except requests.Timeout:
        result = {"parser_status": "Error", "parser_error": "Timeout"}
        return result
    except requests.TooManyRedirects:
        result = {"parser_status": "Error", "parser_error": "TooManyRedirects"}
        return result
    except:
        result = {"parser_status": "Error", "parser_error": "UnexpectedError"}
        return result

    '''
    Get species citation
    '''
    # TODO add 0.5s time delay before second API call
    '''
    try:
        resp_cit = requests.get(url_cit_query, timeout=5)
        resp_cit_j = json.loads(resp_cit.text)
    except requests.ConnectionError:
        result = {"parser_status": "Error", "parser_error": "ConnectionError"}
        return result
    except requests.HTTPError:
        result = {"parser_status": "Error", "parser_error": "HTTPError"}
        return result
    except requests.Timeout:
        result = {"parser_status": "Error", "parser_error": "Timeout"}
        return result
    except requests.TooManyRedirects:
        result = {"parser_status": "Error", "parser_error": "TooManyRedirects"}
        return result
    except:
        result = {"parser_status": "Error", "parser_error": "UnexpectedError"}
        return result
    '''

    if len(resp_j["result"]) > 0:
        result = resp_j["result"][0]

        '''# add data from citation
        if len(resp_cit_j["result"]) > 0:
            result.update(resp_cit_j["result"][0])'''

        # add parser information to result
        result.update({"parser_status": "InRedList", "parser_error": ""})
        return result
    else:
        result = {"parser_status": "Error", "parser_error": "NotInRedList"}
        return result
