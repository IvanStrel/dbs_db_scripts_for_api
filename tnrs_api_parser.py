# coding: utf-8
import json
import requests

def trns_query(genera, species, infra_name):
    """
    Function to make a query to TRNS API and to parse results
    Provided species name will be checked for match with TRNS db
        function will return only accepted names
    :param genera: genera scientific name
    :param species: species scientific name
    :param infra_name: infraspecies scientific name
    :param infra_marker: infraspecies marker
    :return: dictionary with results. Contains elements 'parser status' and 'parser_error'
        and, possibly, resulting elemets from TRNS API response
    """
    # Convert all params to lowercase and remove whitespaces
    genera = genera.lower().replace(" ", "")
    species = species.lower().replace(" ", "")
    infra_name = infra_name.lower().replace(" ", "")

    # Add + sign before infra name
    if len(infra_name) > 0:
        infra_name = " " + infra_name

    # Prepare url for species query
    url_query = "http://tnrs.iplantc.org/tnrsm-svc/matchNames?retrieve=all&names={} {}{}"
    url_query = url_query.format(genera, species, infra_name)

    try:
        resp = requests.get(url_query, timeout=5)
        resp_j = json.loads(resp.text)
    except requests.ConnectionError:
        result = {"parser_status": "Error", "parser_error": "ConnectionError"}
        return result
    except requests.HTTPError:
        result = {"parser_status": "Error", "parser_error": "HTTPError"}
        return result
    except requests.Timeout:
        result = {"parser_status": "Error", "parser_error": "Timeout"}
        return result
    except requests.TooManyRedirects:
        result = {"parser_status": "Error", "parser_error": "TooManyRedirects"}
        return result
    except:
        result = {"parser_status": "Error", "parser_error": "UnexpectedError"}
        return result

    # Get all accepted names from the result
    items = [[obj["acceptedName"], obj["acceptedAuthor"]]
             for obj in resp_j["items"]
             if len(obj["acceptedName"]) > 0]

    if len(items) > 0:
        result = {"items": items}

        # add parser information to result
        result.update({"parser_status": "FoundMatchs", "parser_error": ""})
        return result
    else:
        result = {"parser_status": "Error", "parser_error": "NoMatchedNames"}
        return result
